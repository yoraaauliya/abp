import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import '../../constant.dart';
import '../../dbservices.dart';

class recommendTopDestination extends StatelessWidget {
  const recommendTopDestination({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    
    // DocumentSnapshot dsData =  snapshot.data!.docs[index];
    
    return StreamBuilder<QuerySnapshot>(
      stream: Database.getData(),
      builder: (context, snapshot) {
        DocumentSnapshot dsData = snapshot.data!.docs[1];
        if(snapshot.hasError){
            return Text("ERROR");
        }else if (snapshot.hasData || snapshot.data != null){

       

        }
        return Center(
          child: CircularProgressIndicator(
              valueColor:AlwaysStoppedAnimation<Color>(
                Colors.pinkAccent,) 
            ),
          );
      }
    );
  }
}


// return SingleChildScrollView(
//           scrollDirection: Axis.horizontal,
//           child: Row(
//             children: [
//               recommendDestination(
//                 harga: dsData['harga'],
//                 image: "assets/images/gilitrawangan3.jpg",
//                 title: dsData['title'],
//               ),
//               recommendDestination(
//                 harga: 'Rp100.000',
//                 image: "assets/images/gilitrawangan3.jpg",
//                 title: 'Gili',
//               ),
//               recommendDestination(
//                 harga: 'Rp100.000',
//                 image: "assets/images/gilitrawangan3.jpg",
//                 title: 'Gili',
//               ),
//               recommendDestination(
//                 harga: 'Rp100.000',
//                 image: "assets/images/gilitrawangan3.jpg",
//                 title: 'Gili',
//               ),
//               recommendDestination(
//                 harga: 'Rp100.000',
//                 image: "assets/images/gilitrawangan3.jpg",
//                 title: 'Gili',
//               ),
//             ],
//           ),
//         );

class recommendDestination extends StatelessWidget {
  const recommendDestination({
    Key? key,
    required this.image,
    required this.title,
    required this.harga,
  }) : super(key: key);

  final String image, title, harga;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Container(
      margin: EdgeInsets.only(
          left: kDefaultPadding,
          top: kDefaultPadding / 2,
          bottom: kDefaultPadding),
      width: size.width * 0.4,
      child: Column(
        children: [
          Image(image: AssetImage(image)),
          GestureDetector(
            onTap: () {},
            child: Container(
              padding: EdgeInsets.all(kDefaultPadding / 2),
              decoration: BoxDecoration(color: Colors.white, boxShadow: [
                BoxShadow(
                    offset: Offset(0, 5),
                    blurRadius: 10,
                    color: Theme.of(context).primaryColor.withOpacity(0.25)),
              ]),
              child: Row(
                children: [
                  RichText(
                      text: TextSpan(children: [
                    TextSpan(
                        text: "$title\n", style: TextStyle(color: kTextColor)),
                    TextSpan(
                        text: "Lombok",
                        style: TextStyle(color: kTextColor.withOpacity(0.5))),
                  ])),
                  Spacer(),
                  Text(
                    '$harga',
                    style: TextStyle(color: kTextColor.withOpacity(0.5)),
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
