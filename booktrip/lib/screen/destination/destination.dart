import 'package:booktrip/constant.dart';
import 'package:booktrip/screen/destination/destination_content.dart';
import 'package:flutter/material.dart';

import '../components/header_with_searchbar.dart';

class destination extends StatelessWidget {
  const destination({
    Key? key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final List<String> Data = <String>[
      'Data 0',
      'Data 1',
      'Data 2',
      'Data 3',
      'Data 4',
      'Data 5'
    ];
    return Scaffold(
      body: Column(
        children: [
          HeaderWithSearchBar(size: size),
          SizedBox(height: 4),
          destinationCard(Data: Data),
        ],
      ),
    );
  }
}

class destinationCard extends StatelessWidget {
  const destinationCard({
    Key? key,
    required this.Data,
  }) : super(key: key);

  final List<String> Data;

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Padding(
        padding: const EdgeInsets.fromLTRB(kDefaultPadding, kDefaultPadding / 2,
            kDefaultPadding, kDefaultPadding / 2),
        child: GridView.builder(
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
            ),
            itemCount: Data.length,
            itemBuilder: (context, index) {
              return GestureDetector(
                onTap: () {},
                child: Container(
                  height: 100,
                  width: 100,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10.0),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 1,
                          blurRadius: 5,
                          offset: Offset(1, 4), // changes position of shadow
                        ),
                      ],
                      image: DecorationImage(
                          image: AssetImage("assets/images/gilitrawangan3.jpg"),
                          fit: BoxFit.cover)),
                  child: Padding(
                    padding:
                        const EdgeInsets.only(bottom: 0, top: 192, left: 8),
                    child: Text(
                      Data[index],
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ),
              );
            }),
      ),
    );
  }
}


// return SingleChildScrollView(
//       child: Column(
//         children: [
//           HeaderWithSearchBar(size: size),
//           destinationDetail(
//             title: 'Testing',
//             image: 'assets/images/gilitrawangan3.jpg',
//             size: size,
//           ),
//           destinationDetail(
//             title: 'Testing',
//             image: 'assets/images/gilitrawangan3.jpg',
//             size: size,
//           ),
//           destinationDetail(
//             title: 'Testing',
//             image: 'assets/images/gilitrawangan3.jpg',
//             size: size,
//           ),
//           destinationDetail(
//             title: 'Testing',
//             image: 'assets/images/gilitrawangan3.jpg',
//             size: size,
//           ),
//         ],
//       ),
//     );
