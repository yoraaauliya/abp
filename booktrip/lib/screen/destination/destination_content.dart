import 'package:booktrip/constant.dart';
import 'package:flutter/material.dart';

class destinationDetail extends StatelessWidget {
  const destinationDetail({
    Key? key,
    required this.title,
    required this.size,
    required this.image,
  }) : super(key: key);
  final String title;
  final Size size;
  final String image;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(
          left: kDefaultPadding,
          top: kDefaultPadding / 2,
          bottom: kDefaultPadding),
      width: size.width * 0.4,
      child: Wrap(
        direction: Axis.horizontal,
        children: [
          Image(image: AssetImage(image)),
          GestureDetector(
            onTap: () {},
            child: Container(
              padding: EdgeInsets.all(kDefaultPadding / 2),
              decoration: BoxDecoration(color: Colors.white, boxShadow: [
                BoxShadow(
                    offset: Offset(0, 5),
                    blurRadius: 10,
                    color: Theme.of(context).primaryColor.withOpacity(0.25)),
              ]),
              child: Row(
                children: [Text("Testing")],
              ),
            ),
          )
        ],
      ),
    );
  }
}
