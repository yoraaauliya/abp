import 'package:booktrip/constant.dart';
import 'package:flutter/material.dart';

class timeline_builder extends StatelessWidget {
  timeline_builder({
    Key? key,
    required this.image,
    required this.size,
  }) : super(key: key);

  final List<String> grid = <String>[
    'Grid 0',
    'Grid 1',
    'Grid 2',
    'Grid 3',
    'Grid 4',
    'Grid 5'
  ];
  final String image;
  final Size size;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.builder(
          itemCount: grid.length,
          scrollDirection: Axis.vertical,
          shrinkWrap: true,
          itemBuilder: (context, index) {
            return Padding(
              padding: const EdgeInsets.fromLTRB(kDefaultPadding,
                  kDefaultPadding / 2, kDefaultPadding, kDefaultPadding / 2),
              child: Column(
                children: [
                  Image(
                    image: AssetImage(image),
                    width: size.width,
                    height: size.width / 3.5,
                    fit: BoxFit.cover,
                  ),
                  GestureDetector(
                    onTap: () {},
                    child: Container(
                      padding: EdgeInsets.all(kDefaultPadding / 4),
                      decoration:
                          BoxDecoration(color: Colors.white, boxShadow: [
                        BoxShadow(
                            offset: Offset(0, 5),
                            blurRadius: 10,
                            color: Theme.of(context)
                                .primaryColor
                                .withOpacity(0.25)),
                      ]),
                      child: Row(
                        children: [
                          Text(
                            grid[index],
                            style: TextStyle(
                                color: kTextColor,
                                fontSize: 18,
                                fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            );
          }),
    );
  }
}
